package com.formacion.springbatchdemo.model;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Person {

    private String lastName;
    private String firstName;

}
